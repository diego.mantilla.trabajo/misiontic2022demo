package com.misiontic.DemoCiclo3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCiclo3Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoCiclo3Application.class, args);
	}

}
