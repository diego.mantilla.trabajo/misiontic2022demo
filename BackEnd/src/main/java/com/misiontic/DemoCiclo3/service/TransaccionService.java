/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service;

import com.misiontic.DemoCiclo3.model.Transaccion;
import java.util.List;

/**
 *
 * @author MisionTIC
 */
public interface TransaccionService {
    public Transaccion save(Transaccion transaccion);
    public void delete(Integer id);
    public Transaccion findById(Integer id);
    public List<Transaccion> findAll();
}
