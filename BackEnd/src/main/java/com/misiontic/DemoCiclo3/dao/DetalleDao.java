/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.dao;

import com.misiontic.DemoCiclo3.model.Detalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author MisionTIC
 */
public interface DetalleDao extends CrudRepository<Detalle,Integer> {
    
}
