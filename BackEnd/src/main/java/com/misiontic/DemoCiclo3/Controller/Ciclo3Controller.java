/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package com.misiontic.DemoCiclo3.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author MisionTIC
 */
@RestController
@RequestMapping("/")
public class Ciclo3Controller {
    
    @GetMapping("/demo")
    public ResponseEntity<Object> index()
    {
    return ResponseEntity.ok("Mi primera aplicacion");
    }

}
